use assert_cmd::Command;

const CARGO_AUDIT_REPORT: &str = include_str!("ressources/sast/cargo-audit-sast.json");
const NODEJS_SCAN_AUDIT_REPORT: &str = include_str!("ressources/sast/nodejs-scan-sast.json");

#[test]
fn cargo_audit_sast_report() {
    let mut cmd = Command::cargo_bin("gitlab-report-commentator").unwrap();
    let res = cmd
        .arg("--dry-run") //dry run
        .arg("-t=sast")
        .write_stdin(CARGO_AUDIT_REPORT)
        .assert();
    #[rustfmt::skip]
    res.success().stdout(
        r##"# Vulnerability Report

Hi! The scan reports the following vulnerabilities:
Severity | Name | Message | Source
-------- | ---- | ------- | ------
High | RUSTSEC-2020-0071 | Potential segfault in the time crate |

"##,
    );
}

#[test]
fn nodejs_scan_audit_sast_report() {
    let mut cmd = Command::cargo_bin("gitlab-report-commentator").unwrap();
    let res = cmd
        .arg("--dry-run") //dry run
        .arg("-t=sast")
        .write_stdin(NODEJS_SCAN_AUDIT_REPORT)
        .assert();
    res.success().stdout(
        r##"# Vulnerability Report

Hi! The analyzer [NodeJsScan](https://gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan) reports the following vulnerabilities:
Severity | Name | Message | Source
-------- | ---- | ------- | ------
High | node_secret | A hardcoded secret is identified. Store it properly in an environment variable. | [&lt;anonymized&gt;](&lt;anonymized&gt;#7)
High | vue_template | The Vue.js template has an unescaped variable. Untrusted user input passed to this variable results in Cross Site Scripting (XSS). | [&lt;anonymized&gt;](&lt;anonymized&gt;#19)
Medium | node_insecure_random_generator | crypto.pseudoRandomBytes()/Math.random() is a cryptographically weak random number generator. | [&lt;anonymized&gt;](&lt;anonymized&gt;#191)
Medium | node_username | A hardcoded username in plain text is identified. Store it properly in an environment variable. | [&lt;anonymized&gt;](&lt;anonymized&gt;#7)

"##,
    );
}
