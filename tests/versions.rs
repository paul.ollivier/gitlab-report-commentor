#[test]
fn test_readme_deps() {
    version_sync::assert_only_contains_regex!(
        "README.md",
        "GL_REPORT_COMMENTATOR_VERSION: v{version}"
    );
}
