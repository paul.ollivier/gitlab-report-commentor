use crate::poster::DiffLineComment;
use chrono::NaiveDateTime;
use handlebars::Handlebars;
use log::warn;
use serde::{Deserialize, Serialize};

use super::{ReportFormatHandler, Severity};

pub struct SastHandler;

const VULNERABILITY_REPORT_TEMPLATE: &str =
    include_str!("../../templates/sast/vulnerability_report.md.hbs");
#[cfg(test)]
mod tests {
    use crate::input_formats::sast::SastHandler;
    use crate::input_formats::{ReportFormatHandler, Severity};
    use pretty_assertions::assert_eq;

    const CARGO_AUDIT_REPORT: &str =
        include_str!("../../tests/ressources/sast/cargo-audit-sast.json");
    const NODEJS_SCAN_AUDIT_REPORT: &str =
        include_str!("../../tests/ressources/sast/nodejs-scan-sast.json");
    #[test]
    fn parse_cargo_audit() {
        let doc = SastHandler::parse_to_struct(CARGO_AUDIT_REPORT);
        assert!(doc.scan.is_none()); // weird quirk of gitlab-report
        assert_eq!(doc.vulnerabilities.len(), 1);
        let vuln = doc.vulnerabilities.first().unwrap();
        assert_eq!(vuln.id, None);
        assert_eq!(vuln.name, "RUSTSEC-2020-0071");
        assert_eq!(vuln.message, "Potential segfault in the time crate");
        assert!(!vuln.description.is_empty());
        assert_eq!(vuln.severity, Severity::High);
    }
    #[test]
    fn parse_nodejs_scan() {
        let doc = SastHandler::parse_to_struct(NODEJS_SCAN_AUDIT_REPORT);
        assert!(doc.scan.is_some());
        assert_eq!(doc.vulnerabilities.len(), 4);
        let vuln = doc.vulnerabilities.first().unwrap();
        assert_eq!(
            vuln.id,
            Some("19d9af56f89910253be5b6b383dad335de7367ae4432dcc0c0d4ecf2af045351".to_string())
        );
        assert_eq!(vuln.name, "node_secret");
        assert_eq!(
            vuln.message,
            "A hardcoded secret is identified. Store it properly in an environment variable."
        );
        assert!(!vuln.description.is_empty());
        assert_eq!(vuln.severity, Severity::High);
    }

    #[test]
    fn render_cargo_audit() {
        let comment =
            SastHandler::render_to_markdown(&SastHandler::parse_to_struct(CARGO_AUDIT_REPORT));
        assert_eq!(
            comment,
            r##"# Vulnerability Report

Hi! The scan reports the following vulnerabilities:
Severity | Name | Message | Source
-------- | ---- | ------- | ------
High | RUSTSEC-2020-0071 | Potential segfault in the time crate |
"##
        );
    }
    #[test]
    fn render_nodejs_scan() {
        let comment = SastHandler::render_to_markdown(&SastHandler::parse_to_struct(
            NODEJS_SCAN_AUDIT_REPORT,
        ));
        assert_eq!(
            comment,
            r##"# Vulnerability Report

Hi! The analyzer [NodeJsScan](https://gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan) reports the following vulnerabilities:
Severity | Name | Message | Source
-------- | ---- | ------- | ------
High | node_secret | A hardcoded secret is identified. Store it properly in an environment variable. | [&lt;anonymized&gt;](&lt;anonymized&gt;#7)
High | vue_template | The Vue.js template has an unescaped variable. Untrusted user input passed to this variable results in Cross Site Scripting (XSS). | [&lt;anonymized&gt;](&lt;anonymized&gt;#19)
Medium | node_insecure_random_generator | crypto.pseudoRandomBytes()/Math.random() is a cryptographically weak random number generator. | [&lt;anonymized&gt;](&lt;anonymized&gt;#191)
Medium | node_username | A hardcoded username in plain text is identified. Store it properly in an environment variable. | [&lt;anonymized&gt;](&lt;anonymized&gt;#7)
"##
        );
    }
    #[test]
    fn cargo_audit_to_doc_comment() {
        let comments = SastHandler::render_to_diff_line_comments(&SastHandler::parse_to_struct(
            CARGO_AUDIT_REPORT,
        ));
        assert_eq!(comments.len(), 0); // because we have no valid file set up.
    }
    #[test]
    fn nodejs_scan_to_doc_comment() {
        let comments = SastHandler::render_to_diff_line_comments(&SastHandler::parse_to_struct(
            NODEJS_SCAN_AUDIT_REPORT,
        ));
        assert_eq!(comments.len(), 4);
        let comment = comments.first().unwrap();
        assert_eq!(comment.path, "<anonymized>");
        assert_eq!(comment.line, 7);
    }
}

impl ReportFormatHandler for SastHandler {
    type ReportFormat = SastReport;
    fn parse_to_struct(s: &str) -> Self::ReportFormat {
        serde_json::from_str(s).expect("Could not parse given string as proper SAST report")
    }

    fn render_to_markdown(doc: &Self::ReportFormat) -> String {
        let mut hbs = Handlebars::new();
        hbs.register_template_string(
            "VULNERABILITY_REPORT_TEMPLATE",
            VULNERABILITY_REPORT_TEMPLATE,
        )
        .unwrap();
        hbs.render("VULNERABILITY_REPORT_TEMPLATE", &doc).unwrap()
    }

    fn render_to_diff_line_comments(doc: &Self::ReportFormat) -> Vec<DiffLineComment> {
        let mut res = Vec::new();
        for vuln in &doc.vulnerabilities {
            let Some(path) = &vuln.location.file else {
                warn!("Vulnerability {:?} has no valid file path. Omitting it from the generated report.", &vuln);
                continue;
            };
            let Some(line) = vuln.location.start_line else {
                warn!("Vulnerability {:?} has no valid start_line. Omitting it from the generated report", &vuln);
                continue;
            };
            let body = format!(
                r##"# {severity} - {name}
            Scanner {scanner_name} has found the following:
            {msg}

            {desc}"##,
                severity = vuln.severity,
                name = vuln.name,
                scanner_name = vuln.scanner.name,
                msg = vuln.message,
                desc = vuln.description
            );
            res.push(DiffLineComment {
                path: path.clone(),
                line: line as i64,
                body,
            })
        }
        res
    }
}

#[derive(Deserialize, Serialize, Debug, Default)]
pub struct SastReport {
    version: String,
    vulnerabilities: Vec<Vulnerability>,
    #[serde(default)]
    dependency_files: Vec<()>,
    scan: Option<Scan>,
}

#[derive(Deserialize, Serialize, Debug, Default)]
struct Vulnerability {
    id: Option<String>,
    category: String,
    #[serde(default)]
    name: String,
    message: String,
    description: String,
    #[serde(default)]
    cve: String,
    #[serde(default)]
    severity: Severity,
    #[serde(default)]
    scanner: VulnerabilityScanner,
    #[serde(default)]
    location: VulnerabilityLocation,
    #[serde(default)]
    identifiers: Vec<VulnerabilityIdentifier>,
}

#[derive(Deserialize, Serialize, Debug)]
struct VulnerabilityScanner {
    id: String,
    name: String,
}

impl Default for VulnerabilityScanner {
    fn default() -> Self {
        Self {
            id: "unspecified".to_string(),
            name: "unspecified".to_string(),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Default)]
struct VulnerabilityLocation {
    file: Option<String>,
    start_line: Option<u32>,
    end_line: Option<u32>,
}

#[derive(Deserialize, Serialize, Debug, Default)]
struct VulnerabilityIdentifier {
    #[serde(rename = "type")]
    vulnerability_type: String,
    name: String,
    value: String,
    #[serde(default)]
    url: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
struct Scan {
    #[serde(default)]
    analyzer: Software,
    #[serde(default)]
    scanner: Software,
    #[serde(rename = "type")]
    scan_type: String,
    #[serde(default)]
    start_time: NaiveDateTime,
    #[serde(default)]
    end_time: NaiveDateTime,
    status: String,
}

impl Default for Scan {
    fn default() -> Self {
        Self {
            analyzer: Default::default(),
            scanner: Default::default(),
            scan_type: "unspecified".to_string(),
            start_time: Default::default(),
            end_time: Default::default(),
            status: "unspecified".to_string(),
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
struct Software {
    id: String,
    name: String,
    url: String,
    #[serde(default)]
    vendor: SoftwareVendor,
    version: String,
}

impl Default for Software {
    fn default() -> Self {
        Self {
            id: "unspecified".to_string(),
            name: "unspecified".to_string(),
            url: "unspecified".to_string(),
            vendor: Default::default(),
            version: "unspecified".to_string(),
        }
    }
}

#[derive(Deserialize, Serialize, Debug)]
struct SoftwareVendor {
    name: String,
}

impl Default for SoftwareVendor {
    fn default() -> Self {
        Self {
            name: "unspecified".to_string(),
        }
    }
}
